package main
//don't understand  - got example from here http://rosettacode.org/wiki/Create_a_two-dimensional_array_at_runtime#Go

import (
	"golang.org/x/tour/pic"
)

func Pic(dx, dy int) [][]uint8 {
a := make([][]uint8, dx)
	for i := range a {
		a[i] = make([]uint8, dy)
	}
return a
}

func main() {
	pic.Show(Pic)
}
