package main

import "fmt"

func main() {
	fmt.Println("counting")
	for i := 0; i < 10; i++ {
		//deferred system calls are put into LIFO
		defer fmt.Println(i)
	}

	fmt.Println("done")
}
