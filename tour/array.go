package main

import "fmt"

func main() {
	var a [2]string
	a[0] = "Hell"
	a[1] = "on"
	fmt.Println(a[0], a[1])
	fmt.Println(a)
	fmt.Printf("%+v\n",a)
}
