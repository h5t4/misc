package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("When's Saturday?")
	fmt.Printf("(%T)%d\n",time.Saturday,time.Wednesday)
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("Today.")
	case today + 1:
		fmt.Println("Tomorrow.")
	case today + 2:
		fmt.Println("In two days.")
	default:
		day := today - time.Saturday
		fmt.Printf("%d Too far away.",day)
	}

}
