package main

import (
	"fmt"
	"math"
)

type  Abser interface {
	Abs() float64
}

func main() {
	var a Abser
	var b Abser
	f := MyFloat(-math.Sqrt2)
	v := Vertex{3, 4}
	p := &Vertex{3, 4}

	a = f
	b = &v

	fmt.Printf("raw %T'a' value:%v\n",p,p.X)
	fmt.Printf("'a' Myfloat implements Abser: %v\n",a.Abs())
	fmt.Printf("'b' *Vertex implements Abser: %v\n",b.Abs())
}

type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

type Vertex struct {
	X, Y float64
}

func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}
