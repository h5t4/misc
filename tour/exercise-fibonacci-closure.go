package main

import "fmt"

func fibonacci() func(int) int { //A closure is a function value that references variables from outside its body
	sum := 0
	first := 0
	second := 1
	return  func(x int) int {
		sum = first + second
		first = second
		second = sum
		return sum
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f(i))
	}
}
