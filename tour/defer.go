package main

import "fmt"

func main() {
	defer  fmt.Println("deferred world")
	fmt.Println("Hello")
}
