package main

import (
	"fmt"
	"math"
)


func Sqrt(x float64) float64 {
	//return  math.Sqrt(x)
	z := float64(1)
	for i := 0; i < 100000; i++ {
		z = z - (z  * z - x)/2 * z
	}
	return z
}

func main() {
	var nr float64 = 2
	fmt.Println(Sqrt(nr))
	fmt.Println(math.Sqrt(nr))
}
