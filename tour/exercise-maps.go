package main
//http://codereview.stackexchange.com/questions/27813/split-and-word-count-in-go

import (
	"golang.org/x/tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	string_fields := strings.Fields(s)
	word_count_map := make(map[string]int)
	for _, value := range string_fields {	//You can skip the index or value by assigning to _.
		word_count_map[value]++
	}
	return word_count_map
}

func main() {
	wc.Test(WordCount)
}

