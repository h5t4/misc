package main

import "fmt"

func main() {
	s := []int{2, 3, 4, 5, 7, 11, 13}
	fmt.Println("s ==", s)
	fmt.Printf("(%T)%v\n",s,s)
	for i := 0; i < len(s); i++ {
		fmt.Printf("s[%d] == %d\n",i,s[i]) //slice counting is starting from zero
	}
}

