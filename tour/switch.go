package main

import  (
	"fmt"
	"runtime"
)

func main(){
	fmt.Println("Go runs on")
	switch os := runtime.GOOS ; os {
	case "darwin":
		fmt.Println("OS X.")
	case  "linux":
		fmt.Printf("LINUX -> (%T)%v\n",os,os)
	default:
		fmt.Printf("%s.",os)
	}
}
