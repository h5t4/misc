package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var csv string = "./keyValue.csv"

func readl(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func main() {
	kv, err := readl(csv)
	if err != nil {
		fmt.Fprintf(os.Stderr, "", err)
		os.Exit(1)
	}
	m := make(map[string]string)
	for _, line := range kv {
		s := strings.Fields(line)
		//fmt.Printf("%v,%v\n", s[0], s[1])
		m[s[0]] = s[1]
	}
	fmt.Println(m["key20"])
}
