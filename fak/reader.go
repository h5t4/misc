package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

func main() {
	var r = regexp.MustCompile("p([a-z]+)ch")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		if r.MatchString(scanner.Text()) {
			fmt.Println("regexp match:", scanner.Text())
		}
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}
