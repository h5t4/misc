package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type spk struct {
	position int
	key      string
}

func parseUI(str string) ([]spk, error) {
	s := strings.Split(str, ",")
	var ui []spk
	for _, val := range s {
		res := strings.Split(val, ":")
		ipos, err := strconv.Atoi(res[0])
		if err != nil {
			return nil, fmt.Errorf("parsing %s as int: %v", res[0], err)
		}
		ui = append(ui, spk{
			position: ipos,
			key:      res[1],
		})
	}
	return ui, nil
}

func readl(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func kvfToMap(kvf string) (map[string]string, error) {
	kv, err := readl(kvf)
	if err != nil {
		return nil, err
	}
	m := make(map[string]string)
	for _, line := range kv {
		s := strings.Fields(line)
		var r = regexp.MustCompile("^" + s[0] + "[[:space:]]+")
		//fmt.Println(r.ReplaceAllString(scanner.Text(), m[r.FindString(scanner.Text())]))
		m[s[0]] = r.ReplaceAllString(line, "")
	}
	return m, nil
}

func main() {
	rexp := flag.String("rexp", "", "Regular exppression. For examp. '(foo)([0-9]+)(bar)'")
	kvf := flag.String("kvf", "", "Path to whitespace separated KEY VALUE pairs per line")
	pval := flag.String("pval", "", "Comma separated submatchPosition:optionalHashKey values.")
	flag.Parse()

	switch {
	case *rexp == "":
		fmt.Fprintf(os.Stderr, "--rexp '(foo)([0-9]+)(bar)' \n")
		os.Exit(1)
	case *kvf == "":
		fmt.Fprintf(os.Stderr, "--kvf /path/to/my/key/value/hash/file \n")
		os.Exit(1)
	case *pval == "":
		fmt.Fprintf(os.Stderr, "--pval 1:keyA,2:,3,keyC \n")
		os.Exit(1)
	}

	ui, err := parseUI(*pval)
	if err != nil {
		fmt.Fprintf(os.Stderr, "", err)
		os.Exit(1)
	}

	m, err := kvfToMap(*kvf)
	if err != nil {
		fmt.Fprintf(os.Stderr, "", err)
		os.Exit(1)
	}

	//var r = regexp.MustCompile("p([a-z]+)ch")
	var r = regexp.MustCompile(*rexp)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		//	fmt.Printf("KEY:%v,VALUE:%v", r.FindString(scanner.Text()), m[r.FindString(scanner.Text())])
		//if r.MatchString(scanner.Text()) {
		//	fmt.Println(m[r.FindString(scanner.Text())])
		//fmt.Print("it is true :", scanner.Text())
		//	fmt.Println(r.FindStringSubmatchIndex(scanner.Text()))

		//}
		//fmt.Println(r.ReplaceAllString(scanner.Text(), m[r.FindString(scanner.Text())]))
		//fmt.Println(subMatch(r, scanner.Text(), m[r.FindString(scanner.Text())]))
		fmt.Println(r.ReplaceAllStringFunc(scanner.Text(), func(match string) string {
			//fmt.Println(r.FindStringSubmatch(match))
			//fmt.Println(len(r.FindStringSubmatch(match)))
			parts := r.FindStringSubmatch(match)
			//return parts[1] + complexFunc(parts[2])
			//return parts[1] + subMatch(r, parts[2], m[parts[2]])
			for _, val := range ui {
				//fmt.Printf("val.key:", val.key)
				switch {
				case len(m[val.key]) != 0:
					fmt.Println(1)
					parts[val.position] = m[val.key] //user input treated as hash key to substitute submatch with hash value
				case len(val.key) != 0:
					fmt.Println(2)
					parts[val.position] = val.key //user input treated as literal to substitute submatch.
				case len(m[parts[val.position]]) != 0:
					fmt.Println(3)
					parts[val.position] = m[parts[val.position]] //use submatch as hash key to substitute submatch with hash value.
				default:
					fmt.Println(4)
					fmt.Printf("val.key:", len(val.key))
					//fmt.Printf("m[val.key]:", len(m[val.key]))
				}
			}
			return strings.Join(parts[1:], "")
		}))

	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}
